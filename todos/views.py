from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


# Create your views here.
def todo_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list": list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "detail": details,
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()

    context = {"form": form}
    return render(request, "todos/create_todo_list.html", context)


def update_todo_list(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todos)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todos.id)
    else:
        form = TodoForm(instance=todos)
    context = {
        "form": form,
        "todos": todos,
    }
    return render(request, "todos/update_todo_list.html", context)


def delete_todo_list(request, id):
    delete = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_list_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}
    return render(request, "todos/create_todo_item.html", context)


def update_todo_item(request, id):
    todos = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todos)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=todos)
    context = {
        "form": form,
        "todos": todos,
    }
    return render(request, "todos/update_todo_item.html", context)
